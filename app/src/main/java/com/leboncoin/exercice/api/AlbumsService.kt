package com.leboncoin.exercice.api

import com.leboncoin.exercice.albums.data.Albums
import retrofit2.Response
import retrofit2.http.GET

/**
 * Albums REST API access points
 */
interface AlbumsService {

    companion object {
        const val ENDPOINT = "https://static.leboncoin.fr/"
    }

    @GET("img/shared/technical-test.json")
    suspend fun getAlbums(): Response<List<Albums>>

}
