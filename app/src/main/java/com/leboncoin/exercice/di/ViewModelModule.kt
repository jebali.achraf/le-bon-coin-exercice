package com.leboncoin.exercice.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.leboncoin.exercice.albums.ui.AlbumsViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(AlbumsViewModel::class)
    abstract fun bindThemeViewModel(viewModel: AlbumsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
