package com.leboncoin.exercice.di


import com.leboncoin.exercice.albums.ui.AlbumsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeThemeFragment(): AlbumsFragment
}
