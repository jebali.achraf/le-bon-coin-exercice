package com.leboncoin.exercice.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
