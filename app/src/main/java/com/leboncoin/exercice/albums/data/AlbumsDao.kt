package com.leboncoin.exercice.albums.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * The Data Access Object for the Albums class.
 */
@Dao
interface AlbumsDao {

    @Query("SELECT * FROM albums ORDER BY id DESC")
    fun getAlbumss(): LiveData<List<Albums>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plants: List<Albums>)
}
