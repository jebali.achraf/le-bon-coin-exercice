package com.leboncoin.exercice.albums.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "albums")
data class Albums(
        @PrimaryKey
        @field:SerializedName("id")
        val id: Int,
        @field:SerializedName("title")
        val title: String,
        @field:SerializedName("thumbnailUrl")
        val thumbnailUrl: String
) {
    override fun toString() = title
}