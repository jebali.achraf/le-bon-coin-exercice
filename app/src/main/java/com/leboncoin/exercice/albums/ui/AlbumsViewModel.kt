

package com.leboncoin.exercice.albums.ui

import androidx.lifecycle.ViewModel
import com.leboncoin.exercice.albums.data.AlbumsRepository
import javax.inject.Inject

/**
 * The ViewModel for [AlbumsFragment].
 */
class AlbumsViewModel @Inject constructor(repository: AlbumsRepository) : ViewModel() {

    val albums= repository.albums
}
