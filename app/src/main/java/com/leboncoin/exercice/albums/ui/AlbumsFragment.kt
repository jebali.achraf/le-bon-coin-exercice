package com.leboncoin.exercice.albums.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.leboncoin.exercice.R
import com.leboncoin.exercice.data.Result
import com.leboncoin.exercice.databinding.FragmentAlbumsBinding
import com.leboncoin.exercice.di.Injectable
import com.leboncoin.exercice.di.injectViewModel
import com.leboncoin.exercice.ui.VerticalItemDecoration
import com.leboncoin.exercice.ui.hide
import com.leboncoin.exercice.ui.show
import javax.inject.Inject

class AlbumsFragment : Fragment(), Injectable {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: AlbumsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        viewModel = injectViewModel(viewModelFactory)

        val binding = FragmentAlbumsBinding.inflate(inflater, container, false)
        context ?: return binding.root

        val adapter = AlbumsAdapter()
        binding.recyclerView.addItemDecoration(
                VerticalItemDecoration(resources.getDimension(R.dimen.margin_normal).toInt(), true) )
        binding.recyclerView.adapter = adapter

        subscribeUi(binding, adapter)

        setHasOptionsMenu(true)
        return binding.root
    }

    private fun subscribeUi(binding: FragmentAlbumsBinding, adapter: AlbumsAdapter) {
        viewModel.albums.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    binding.progressBar.hide()
                    result.data?.let { adapter.submitList(it) }
                }
                Result.Status.LOADING -> binding.progressBar.show()
                Result.Status.ERROR -> {
                    binding.progressBar.hide()
                    Snackbar.make(binding.root, result.message!!, Snackbar.LENGTH_LONG).show()
                }
            }
        })
    }
}