

package com.leboncoin.exercice.albums.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.leboncoin.exercice.databinding.ListItemAlbumBinding
import com.leboncoin.exercice.albums.data.Albums
import com.squareup.picasso.Picasso

/**
 * Adapter for the [RecyclerView] in [AlbumsFragment].
 */
class AlbumsAdapter : ListAdapter<Albums, AlbumsAdapter.ViewHolder>(DiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val Albums = getItem(position)
        holder.apply {
            bind(Albums)
            itemView.tag = Albums
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ListItemAlbumBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
    }

    class ViewHolder(
        private val binding: ListItemAlbumBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Albums) {

            Picasso.get().load(item.thumbnailUrl).into(binding.image)
            binding.apply {
                album = item
                executePendingBindings()
            }
        }
    }
}

private class DiffCallback : DiffUtil.ItemCallback<Albums>() {

    override fun areItemsTheSame(oldItem: Albums, newItem: Albums): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Albums, newItem: Albums): Boolean {
        return oldItem == newItem
    }
}