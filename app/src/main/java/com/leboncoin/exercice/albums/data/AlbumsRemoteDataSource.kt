package com.leboncoin.exercice.albums.data

import com.leboncoin.exercice.api.BaseDataSource
import com.leboncoin.exercice.api.AlbumsService
import javax.inject.Inject

/**
 * Works with the Albums API to get data.
 */
class AlbumsRemoteDataSource @Inject constructor(private val service: AlbumsService) : BaseDataSource() {

    suspend fun fetchData() = getResult { service.getAlbums() }

}
