package com.leboncoin.exercice.albums.data

import com.leboncoin.exercice.data.resultLiveData
import com.leboncoin.exercice.testing.OpenForTesting
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository module for handling data operations.
 */
@Singleton
@OpenForTesting
class AlbumsRepository @Inject constructor(private val dao: AlbumsDao,
                                              private val remoteSource: AlbumsRemoteDataSource) {

    val albums = resultLiveData(
            databaseQuery = { dao.getAlbumss() },
            networkCall = { remoteSource.fetchData() },
            saveCallResult = { dao.insertAll(it) })

}
