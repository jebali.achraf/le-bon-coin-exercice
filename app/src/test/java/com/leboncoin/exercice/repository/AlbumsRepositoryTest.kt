package com.leboncoin.exercice.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.leboncoin.exercice.api.AlbumsService
import com.leboncoin.exercice.data.AppDatabase
import com.leboncoin.exercice.albums.data.AlbumsDao
import com.leboncoin.exercice.albums.data.AlbumsRemoteDataSource
import com.leboncoin.exercice.albums.data.AlbumsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

@RunWith(JUnit4::class)
class AlbumsSetRepositoryTest {
    private lateinit var repository: AlbumsRepository
    private val dao = mock(AlbumsDao::class.java)
    private val service = mock(AlbumsService::class.java)
    private val remoteDataSource = AlbumsRemoteDataSource(service)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    @Before
    fun init() {
        val db = mock(AppDatabase::class.java)
        `when`(db.AlbumsDao()).thenReturn(dao)
        `when`(db.runInTransaction(ArgumentMatchers.any())).thenCallRealMethod()
        repository = AlbumsRepository(dao, remoteDataSource)
    }

    @Test
    fun loadAlbumsSetsFromNetwork() {
        runBlocking {
            repository.albums

            verify(dao, never()).getAlbumss()
            verifyZeroInteractions(dao)
        }
    }

}