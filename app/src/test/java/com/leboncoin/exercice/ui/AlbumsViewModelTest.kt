package com.leboncoin.exercice.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.leboncoin.exercice.albums.data.AlbumsRepository
import com.leboncoin.exercice.albums.ui.AlbumsViewModel
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class AlbumsSetViewModelTest {

    private val themeId = 567

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()
    private val repository = mock(AlbumsRepository::class.java)
    private var viewModel = AlbumsViewModel(repository)

    @Test
    fun testNull() {
        assertThat(viewModel.albums, nullValue())
    }
}