package com.leboncoin.exercice.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class AlbumsServiceTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: AlbumsService

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
                .baseUrl(mockWebServer.url(""))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(AlbumsService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun requestAlbumsSets() {
        runBlocking {
            enqueueResponse("technical-test.json")
            val resultResponse = service.getAlbums().body()

            val request = mockWebServer.takeRequest()
            assertNotNull(resultResponse)
            assertThat(request.path, `is`("img/shared/"))
        }
    }

    @Test
    fun getAlbumsSetsResponse() {
        runBlocking {
            enqueueResponse("technical-test.json")
            val resultResponse = service.getAlbums().body()

            assertThat(resultResponse!!.count(), `is`(5000))
        }
    }


    @Test
    fun getAlbumsSetItem() {
        runBlocking {
            enqueueResponse("technical-test.json")
            val resultResponse = service.getAlbums().body()

            val albums = resultResponse!![0]
            assertThat(albums.id, `is`(1))
            assertThat(albums.title, `is`("accusamus beatae ad facilis cum similique qui sunt"))
            assertThat(albums.thumbnailUrl, `is`("https://via.placeholder.com/150/92c952"))
        }
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader
                .getResourceAsStream("api-response/$fileName")
        val source = Okio.buffer(Okio.source(inputStream))
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(mockResponse.setBody(
                source.readString(Charsets.UTF_8))
        )
    }
}
