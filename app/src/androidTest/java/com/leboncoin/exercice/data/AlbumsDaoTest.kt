package com.leboncoin.exercice.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.leboncoin.exercice.albums.data.AlbumsDao
import com.leboncoin.exercice.util.getValue
import com.leboncoin.exercice.util.testAlbumsSetA
import com.leboncoin.exercice.util.testAlbumsSetB
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AlbumsSetDaoTest : DbTest() {
    private lateinit var albumsDao: AlbumsDao
    private val setA = testAlbumsSetA.copy()
    private val setB = testAlbumsSetB.copy()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before fun createDb() {
        albumsDao = db.AlbumsDao()

        // Insert albumss in non-alphabetical order to test that results are sorted by name
        runBlocking {
            albumsDao.insertAll(listOf(setA, setB))
        }
    }

    @Test fun testGetSets() {
        val list = getValue(albumsDao.getAlbumss())
        assertThat(list.size, equalTo(2))

        // Ensure albums list is sorted by name
        assertThat(list[0], equalTo(setA))
        assertThat(list[1], equalTo(setB))
    }
}