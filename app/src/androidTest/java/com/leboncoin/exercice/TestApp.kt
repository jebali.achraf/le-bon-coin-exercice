package com.leboncoin.exercice

import android.app.Application

/**
 * App for tests to prevent initializing dependency injection.
 *
 * See [com.leboncoin.exercice.util.AppTestRunner].
 */
class TestApp : Application()
